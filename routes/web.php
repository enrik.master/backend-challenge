<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
	

	Route::get('/', function () {
	    return view('index');
	});
	
	Route::get('/obtener-posts', '\App\Http\Controllers\ApiFetchController@getPosts')->name('get-posts');
	Route::get('/obtener-usuarios', '\App\Http\Controllers\ApiFetchController@getUsers')->name('get-users');
	
	Route::get('/users', '\App\Http\Controllers\ApiController@index')->name('users');
	Route::get('/posts/top', '\App\Http\Controllers\ApiController@topPosts')->name('top-posts');
	Route::get('/posts/{id}', '\App\Http\Controllers\ApiController@show')->name('top-posts');
