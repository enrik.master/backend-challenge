<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('users', function (Blueprint $table) {
		    $table->increments('id');
		    
		    $table->string('name', 100);
		    $table->string('email', 100);
		    $table->string('password', 100)->nullable();
		    $table->string('city', 100);
		
		    $table->timestamps();
		    
	    });
	
	    Schema::table('posts',  function (Blueprint $table) {
		    $table->foreign('user_id')->references('id')->on('users')
			    ->onUpdate('cascade')
			    ->onDelete('set null');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('posts',  function (Blueprint $table) {
		    $table->dropForeign('users_user_id_foreign');
	    });
	    
        Schema::dropIfExists('users');
    }
}
