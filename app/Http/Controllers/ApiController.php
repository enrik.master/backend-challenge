<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
	
	
	public function topPosts(){
		
		$json = Post::select('posts.id', 'posts.body', 'posts.title', DB::raw('MAX(posts.rating) as rating'),'posts.user_id','users.name')
			->leftJoin('users', 'users.id', '=', 'posts.user_id')
			->orderByDesc('posts.rating')
			->groupBy('posts.user_id')->get();
		
		return $json;
	}
	
	
	
    public function index(){
	    
    	//obtenemos los datos que queremos sin ordenar
    	$json = User::with(['posts' => function ($query) {
		    $query->select('id', 'user_id', 'body', 'title');
	    }])->get(['id', 'name', 'email', 'city']);
	
	
		//obtenemos los id ordenados por puntuacion
	    $sortedIds =  DB::table('posts')
		    ->select('user_id',DB::raw('AVG(rating) as media'))
		    ->groupBy('user_id')
		    ->orderByDesc('media')
		    ->get()
		    ->pluck('user_id')
		    ->toArray();
	    
	    
    	//ordenamos la coleccion pasandole los ids ordenados, gracias a la potencia del sortBy de laravel
	    $sortedJson = $json->sortBy(function($order) use($sortedIds){
		    return array_search($order['id'], $sortedIds);
	    })->values()->all();
    	
	    
	    //devolvemos el JSON odenado
	    return $sortedJson;
    }
	
	public function show($id){
		
		$json = Post::query()->select('posts.id', 'posts.body', 'posts.title', 'users.name')
			->leftJoin('users', 'users.id', '=', 'posts.user_id')
			->where('posts.id','=',$id)
			->get();
		
		if($json->isNotEmpty())
			return $json;
		
		abort(404);
		
	}
	
	
}
