<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use GuzzleHttp\Client;

class ApiFetchController extends Controller
{
	
	public function getPosts() {
		
		$client = new Client();

		$response = $client->request('GET', env('API_ENDPOINT')."/posts?_limit=50");
		$posts = json_decode($response->getBody(), true);
		
		$accion = 1;
		
		foreach($posts as $item){
			
			if(Post::where('id','=',$item["id"])->count()){
				
				$post = Post::where('id', '=', $item["id"])->first();
				$post->body = $item["body"];
				$post->save();
				
				$accion = 2;
				
			}else{
				
				$item['rating'] = ( str_word_count($item['title']) * 2 ) + str_word_count($item['body']);
				
				$item['user_id'] = $item['userId'];
				unset($item['userId']);
				
				$post = new Post($item);
				$post->save();
				
			}
			
		}
		
		return view('obtenerPosts',['accion' => $accion]);
	}
	
	
	public function getUsers() {
		
		$client = new Client();
		
		$posts = Post::all();
		
		$insertados = false;
		
		foreach($posts as $post){
			
			$response = $client->request('GET', env('API_ENDPOINT')."/users/" . $post['user_id']);
			$userData = json_decode($response->getBody(), true);
			
			if(!User::where('id','=',$userData["id"])->count()){
				
				$arrayUserData = [
					'name' => $userData['name'],
					'email' => $userData['email'],
					'city' => $userData['address']['city']
				];
				
				$user = new User($arrayUserData);
				$user->save();
				
				$insertados = true;
			}
			
		}
		
		return view('obtenerUsuarios',['insertados' => $insertados]);
	}
}
